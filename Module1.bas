Attribute VB_Name = "Module1"
Option Explicit

Private Declare Function WritePrivateProfileString _
    Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpString As Any, _
        ByVal lpFileName As String) As Long

Private Declare Function GetPrivateProfileString _
    Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpDefault As String, _
        ByVal lpReturnedString As String, _
        ByVal nSize As Long, _
        ByVal lpFileName As String) As Long

Private Declare Function sndPlaySound Lib "winmm" _
    Alias "sndPlaySoundA" ( _
    ByVal lpszSound As String, _
    ByVal fuSound As Long) As Boolean

Private Const SND_SYNC As Long = &H0
Private Const SND_ASYNC As Long = &H1
Private Const SND_NODEFAULT As Long = &H2
Private Const SND_MEMORY As Long = &H4
Private Const SND_LOOP As Long = &H8
Private Const SND_NOSTOP As Long = &H10


'定数宣言
Const APP_TITLE As String = "PROGRAM89"
Const KEY_HISCORE As String = "HISCORE"
Const KEY_USESOUND As String = "USESOUND"
Const KEY_SNDFILES As String = "SOUNDFILE"
Const Save_File_Title As String = "HISCORE.ini"

'保存するデータのサイズ
Const String_Size As Integer = 100

'保存するファイル名
Dim Init_File_Path As String

Public NoSound As Boolean
Public SoundFiles(15) As String

Public Enum WhatSound
    BOMB1 = 0
    BOMB2
    SHOT1
    SHOT2
    SHOT3
    SHOT4
    SPEEDUP
    BARRIER
    DAMAGE
    SHOT5
    GUARD
End Enum


Function Trim0(s As String) As String
  Dim tmp As String, i As Integer
  
  tmp = ""
  For i = 1 To Len(s)
    If Asc(Mid(s, i, 1)) = 0 Then
      Exit For
    End If
    tmp = tmp & Mid(s, i, 1)
  Next i
  
  Trim0 = tmp
  
End Function

Function WPPS( _
          lpApplicationName0 As String, _
          lpKeyName0 As String, _
          lpString0 As String) As Long
  
  WPPS = WritePrivateProfileString( _
                  lpApplicationName0, _
                  lpKeyName0, _
                  lpString0, _
                  Init_File_Path)
        
End Function
        
Function GPPS( _
            lpApplicationName0 As String, _
            lpKeyName0 As String, _
            lpDefault0 As String, _
            lpReturnedString0 As String) As Long
  
  Dim tmp As String * String_Size
  
  GPPS = GetPrivateProfileString( _
                lpApplicationName0, _
                lpKeyName0, _
                lpDefault0, _
                tmp, _
                String_Size, _
                Init_File_Path)
                
  lpReturnedString0 = Trim0(tmp)
  
End Function
        
Public Sub Init_HiScoreFile()
    Dim p As String
    
    p = App.Path
    If Right$(p, 1) <> "\" Then
        p = p & "\"
    End If
    
    Init_File_Path = p & Save_File_Title

End Sub


Public Function GetHiScore() As Long
    Dim s As String
    
    GPPS APP_TITLE, KEY_HISCORE, "0", s
    
    GetHiScore = CLng(Val(s))
        
End Function

Public Sub SaveHiScore(sc As Long)
    Dim s As String
    
    s = Format(sc)
    
    WPPS APP_TITLE, KEY_HISCORE, s
    
End Sub

Public Sub GetSoundFiles()
    Dim i As Integer
    Dim s As String, Key1 As String
    
    GPPS APP_TITLE, KEY_USESOUND, CStr(False), s
    NoSound = CBool(s)
    
    For i = 0 To UBound(SoundFiles)
        Key1 = KEY_SNDFILES & i
        GPPS APP_TITLE, Key1, " ", s
        SoundFiles(i) = Trim$(s)
    Next i
    
End Sub

Public Sub SaveSoundFiles()
    Dim i As Integer
    Dim s As String, Key1 As String
    
    s = CStr(NoSound)
    WPPS APP_TITLE, KEY_USESOUND, s
    
    For i = 0 To UBound(SoundFiles)
        s = SoundFiles(i)
        Key1 = KEY_SNDFILES & i
        WPPS APP_TITLE, Key1, s
    Next i
    
End Sub

Public Sub SoundTest(str1 As String)
    sndPlaySound str1, SND_SYNC
End Sub

Public Sub PlaySound(wsd As WhatSound)
    
    If NoSound Then
        Exit Sub
    End If
    
    sndPlaySound SoundFiles(wsd), SND_ASYNC
    
End Sub
