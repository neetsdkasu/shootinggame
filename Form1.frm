VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "シューティングゲーム"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   6360
   Icon            =   "Form1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   224
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   424
   StartUpPosition =   2  '画面の中央
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   6345
      Top             =   900
   End
   Begin VB.PictureBox Picture2 
      BackColor       =   &H00000000&
      Height          =   3060
      Left            =   150
      ScaleHeight     =   200
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   400
      TabIndex        =   1
      Top             =   150
      Width           =   6060
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   540
      Left            =   6315
      Picture         =   "Form1.frx":030A
      ScaleHeight     =   32
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   64
      TabIndex        =   0
      Top             =   225
      Visible         =   0   'False
      Width           =   1020
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000016&
      X1              =   0
      X2              =   424
      Y1              =   1
      Y2              =   1
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000003&
      X1              =   0
      X2              =   424
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー(&M)"
      Begin VB.Menu mnuHelp 
         Caption         =   "ヘルプ(&H)..."
      End
      Begin VB.Menu mnuBar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSetting 
         Caption         =   "設定(&S)..."
      End
      Begin VB.Menu mnuBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHiscoreClear 
         Caption         =   "ハイスコアのクリア(&C)"
      End
      Begin VB.Menu mnuBar3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'cc 自機の状態　0-キー入力可能 1-キー入力不可 2-破壊
'cb 自機爆破後の処理用、 　cx,cy 自機の左上座標
Dim cc As Integer, cb As Integer
Dim cx As Single, cy As Single
'攻撃１　通常弾
'ss 攻撃１の各弾の有無、　sn 攻撃１の射出残存弾数
'sx,sy 攻撃１の各弾の中心座標、sr 攻撃１の弾の大きさ、sv 攻撃１の弾の速度
Dim ss(9) As Integer, sn As Integer
Dim sx(9) As Single, sy(9) As Single, sr As Single, sv As Single
'攻撃２　３方向弾
'tt 攻撃２の各弾の有無、　tn 攻撃２の射出残存弾数、tm 攻撃２の点数計算用
'tx,ty 攻撃２の各弾の中心座標、tr 攻撃２の弾の大きさ、tv 攻撃２の弾の速度
Dim tt(5) As Integer, tn As Integer ', tm As Integer
Dim tx(5) As Single, ty(5) As Single, tr As Single, tv As Single
Dim tvy(5) As Single
'攻撃３　貫通弾
'zz 攻撃３の弾の有無、　zn 攻撃３の残存弾数、 zs 攻撃３の弾追加ボーナスの計算用
'zx,zy 攻撃３の弾の中心座標、zr 攻撃３の弾の大きさ、zv 攻撃３の弾の速度
Dim zz As Integer, zn As Integer, zs As Long
Dim zx As Single, zy As Single, zr As Single, zv As Single
'ブースター
'bb ブースターを発動可能回数、 bt ブースターの残りエネルギー
'bn ブースターの発動回数追加ボーナスの計算用
Dim bb As Integer, bt As Integer, bn As Integer
'攻撃４　追跡弾
'uu 攻撃４の各弾の有無、uk 攻撃４の各弾の攻撃目標の状態
'um 攻撃４の残弾数、ue 攻撃４の射出残存弾数
'ux1,uy1 攻撃４の各弾の中心座標、ur 攻撃４の弾の大きさ
'ux2,uy2,ux3,uy3 攻撃４の各弾の弾道跡の座標、uvx,uvy 攻撃４の各弾の速度
Dim uu(4) As Integer, uk(4) As Integer, um As Integer, ue As Integer
Dim ux1(4) As Single, uy1(4) As Single, ur As Single
Dim ux2(4) As Single, uy2(4) As Single, ux3(4) As Single, uy3(4) As Single
Dim uvx(4) As Single, uvy(4) As Single

Dim vv As Integer, vn As Integer, vr As Single

'敵機（ボス）登場の計算用
Dim BossScore As Long

'敵機爆破の描写用
Dim pp(15) As Integer, ps(15) As Long, pk(15) As Integer
Dim px(15) As Single, py(15) As Single, pc(15) As Long, pr(15) As Single

'敵機の弾
Dim qq(20) As Integer, qc(20) As Long
Dim qx(20) As Single, qy(20) As Single, qr(20) As Single
Dim qvx(20) As Single, qvy(20) As Single

'点数
Dim Score As Long, HiScore As Long

Dim gFlag As Boolean

'敵機の各種情報
Dim dd(4) As DanGan

Dim teki(10) As Integer
Dim geki(10) As Integer
Dim deru(10) As Integer
Dim teki_name(10) As String
Dim teki_max(10) As Integer
Dim teki_score(10) As Long

Dim hatsu(5) As Integer
Dim atari(5) As Integer
Dim wepon(5) As String
Dim Hyoka As Long

Dim Stars(80) As Star

Private Type Star
    X As Single
    Y As Single
    k As Integer
    V As Single
    C As Long
End Type

Private Type DanGan
    dn As Integer
    dx As Single
    dy As Single
    dk As Integer
    dr As Single
    dc As Long
    ds As Long
    dv As Single
End Type

Private Sub Init_Game()
    Dim i As Integer
    
    cc = 0: bb = 5: bn = 0: bt = 0
    cx = 100: cy = 100
    zn = 1: zs = 0
    um = 300: ue = 0
    tn = 6
    sn = 0
    vv = 0: vn = 30
    
    gFlag = True
    
    BossScore = 0
    For i = 0 To 5
        tt(i) = 0
    Next i
    
    For i = 0 To 9
        ss(i) = 0
    Next i
    
    For i = 0 To 4
        dd(i).dn = 0
        uu(i) = 0
    Next i
    Score = 0
    
    For i = 0 To 5
        hatsu(i) = 0
        atari(i) = 0
    Next i
    
    For i = 0 To 6
        teki(i) = teki_max(i)
        geki(i) = 0
        deru(i) = 0
        teki_score(i) = 0
    Next i
    
    For i = 0 To 15
        pp(i) = 0
    Next i
    
    For i = 0 To 20
        qq(i) = 0
    Next i
    
    Timer1.Enabled = True
    mnuHiscoreClear.Enabled = True
    mnuSetting.Enabled = True

End Sub

Function Kyori(x1 As Single, y1 As Single, x2 As Single, y2 As Single) As Single
    
    Kyori = Sqr((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
    
End Function

Sub Byosha()
    Dim i As Integer, C As Long
    Dim X As Single, Y As Single, R As Single
    Dim n0 As Long, n1 As Long, n2 As Long, n3 As Long
    Dim n4 As Long
    Dim rnk As Long, rnk_p As String
    
    
    Picture2.Refresh
    
    Picture2.AutoRedraw = False
    
    For i = 0 To 79
        Picture2.PSet (Stars(i).X, Stars(i).Y), Stars(i).C
        If Stars(i).k = 1 Then
            Picture2.Circle (Stars(i).X, Stars(i).Y), 1, vbWhite
        End If
    Next i
    
    If gFlag Then
        Picture2.CurrentX = 115
        Picture2.CurrentY = 100
        Picture2.ForeColor = vbWhite
        Picture2.Print "PUSH SPACE KEY TO START"
        Exit Sub
    End If
    
    
    '敵の描写
    For i = 0 To 4
        If dd(i).dn > 0 Then
            Picture2.Circle (dd(i).dx, dd(i).dy), dd(i).dr, dd(i).dc
        End If
    Next i
    
    '敵弾の描写
    For i = 0 To 20
        If qq(i) > 0 Then
            Picture2.FillColor = qc(i)
            Picture2.FillStyle = vbFSSolid
            Picture2.Circle (qx(i), qy(i)), qr(i), qc(i)
            Picture2.FillStyle = vbFSTransparent
        End If
    Next i
    
    '攻撃１の描写
    If sn > 0 Then
        For i = 0 To 9
            If ss(i) > 0 Then
                Picture2.Circle (sx(i), sy(i)), sr, vbYellow
            End If
        Next i
    End If
    
    '攻撃２の描写
    If tn < 6 Then
        For i = 0 To 5
            If tt(i) > 0 Then
                Picture2.Circle (tx(i), ty(i)), tr, vbGreen
            End If
        Next i
    End If
    
    '攻撃３の描写
    If zz > 0 Then
        Picture2.FillStyle = vbFSSolid
        Picture2.FillColor = vbWhite
        Picture2.Circle (zx, zy), zr, vbWhite
        Picture2.FillStyle = vbFSTransparent
    End If
    
    If ue > 0 Then
        For i = 0 To 4
            If uu(i) > 0 Then
                Picture2.Line (ux3(i), uy3(i))-(ux2(i), uy2(i)), RGB(128, 128, 128)
                Picture2.Line (ux1(i), uy1(i))-(ux2(i), uy2(i)), RGB(196, 196, 196)
                Picture2.Circle (ux1(i), uy1(i)), ur, vbWhite
            End If
        Next i
    End If
    
    If vv > 0 Then
        Picture2.FillStyle = vbDiagonalCross
        Picture2.FillColor = RGB(0, 255, 128)
        Picture2.Circle (cx + 16, cy + 16), vr, RGB(0, 255, 128)
        Picture2.FillStyle = vbFSTransparent
    End If
    
    
    If cc = 2 Then
        '自機撃墜の描写
        If cb > 0 Then
            For i = 0 To 4
                X = Rnd() * 32 + cx
                Y = Rnd() * 32 + cy
                R = Rnd() * 16
                Picture2.Circle (X, Y), R, vbWhite
            Next i
            cb = cb - 1
            Picture2.CurrentX = cx
            Picture2.CurrentY = cy
            Picture2.ForeColor = vbRed
            Picture2.Print "Bomb"
            cx = cx - 5
        End If
    Else
        '自機の描写
        If bt > 0 Then
            C = RGB(bt * 12, bt * 12, bt * 12)
            Picture2.Circle (cx + 16, cy + 16), 18, C
            R = Rnd()
            For i = 0 To 8
                R = R + Rnd() * 2
                X = 18 * Cos(R) + cx + 16
                Y = 18 * Sin(R) + cy + 16
                Picture2.Line (cx + 16, cy + 16)-(X, Y), C
            Next i
        End If
        Picture2.PaintPicture Picture1.Image, cx, cy, , , 32, 0, 32, 32, vbSrcAnd
        Picture2.PaintPicture Picture1.Image, cx, cy, , , 0, 0, 32, 32, vbSrcPaint
    End If
    
    
    '各種文字表示
    Picture2.CurrentY = 0
    Picture2.CurrentX = 0
    Picture2.ForeColor = vbWhite
    Picture2.Print "Score "; Score
    Picture2.CurrentX = 0
    Picture2.ForeColor = vbYellow
    Picture2.Print "F "; 10 - sn
    Picture2.CurrentX = 0
    Picture2.ForeColor = vbGreen
    Picture2.Print "D "; tn
    Picture2.CurrentX = 0
    Picture2.ForeColor = vbWhite
    Picture2.Print "S "; zn
    Picture2.CurrentX = 0
    Picture2.ForeColor = RGB(128, 128, 128)
    Picture2.Print "E "; bb; bt
    Picture2.CurrentX = 0
    Picture2.ForeColor = RGB(128, 128, 128)
    Picture2.Print "m "; um; ue
    Picture2.CurrentX = 0
    Picture2.ForeColor = RGB(128, 128, 128)
    Picture2.Print "V "; vn; vv
        
    If (cc = 2) And (cb = 0) Then
        n0 = 0: n1 = 0: n2 = 0: n3 = 0: n4 = 0
        For i = 0 To 6
            n0 = n0 + geki(i) * (Hyoka / teki_max(i))
            n1 = n1 + Hyoka
            n2 = n2 + geki(i)
            n3 = n3 + teki_max(i)
            n4 = n4 + deru(i)
        Next i
        
        rnk = (1000 * n0) / n1
        If rnk > 900 Then
            rnk_p = "SSS"
        ElseIf rnk > 800 Then
            rnk_p = "SS"
        ElseIf rnk > 700 Then
            rnk_p = "S"
        ElseIf rnk > 500 Then
            rnk_p = "A"
        ElseIf rnk > 400 Then
            rnk_p = "B"
        ElseIf rnk > 300 Then
            rnk_p = "C"
        ElseIf rnk > 200 Then
            rnk_p = "D"
        ElseIf rnk > 100 Then
            rnk_p = "E"
        Else
            rnk_p = "F"
        End If
        
        Picture2.Line (65, 20)-(380, 180), vbBlack, BF
        
        Picture2.CurrentY = 20
                
        Picture2.ForeColor = vbWhite
        
        Picture2.CurrentX = 65
        Picture2.Print "Score "; Score;
        Picture2.CurrentX = 305
        Picture2.Print "Rank "; rnk_p
        
        Picture2.ForeColor = RGB(128, 128, 128)
        
        Picture2.CurrentX = 65
        Picture2.Print "Total";
        Picture2.CurrentX = 130: Picture2.Print Right("  " & n2, 3);
        Picture2.CurrentX = 155: Picture2.Print "/   "; Right("   " & n4, 4);
        Picture2.CurrentX = 200: Picture2.Print "/  "; Right("   " & n3, 4);
        Picture2.CurrentX = 260: Picture2.Print Right("  " & Format(n2 / n4 * 100, "0.0"), 5) & "%";
        Picture2.CurrentX = 305
        Picture2.Print Right("  " & Format(n2 / n3 * 100, "0.0"), 5) & "%"
        
        For i = 0 To 6
            Picture2.CurrentX = 65: Picture2.Print teki_name(i);
            Picture2.CurrentX = 130: Picture2.Print Right("  " & geki(i), 3);
            Picture2.CurrentX = 155: Picture2.Print "/  "; Right("   " & deru(i), 4);
            Picture2.CurrentX = 200: Picture2.Print "/   "; Right("  " & teki_max(i), 3);
            Picture2.CurrentX = 260
            If deru(i) > 0 Then
                Picture2.Print Right("  " & Format(geki(i) / deru(i) * 100, "0.0"), 5) & "%";
            Else
                Picture2.Print "  0.0%";
            End If
            Picture2.CurrentX = 305
            Picture2.Print Right("  " & Format(geki(i) / teki_max(i) * 100, "0.0"), 5) & "%";
            Picture2.CurrentX = 340
            Picture2.Print "... "; teki_score(i)
        Next i
        
        Picture2.CurrentX = 65
        For i = 0 To 5
            Picture2.Print wepon(i); Right("    " & atari(i), 5);
            Picture2.Print "/"; Right("    " & hatsu(i), 5); " ... ";
            If hatsu(i) > 0 Then
                Picture2.Print Right("     " & Format$(atari(i) / hatsu(i) * 100, "0.0"), 8) & "%",
            Else
                Picture2.Print "     0.0%",
            End If
            If (i Mod 2) = 1 Then
                Picture2.Print
                Picture2.CurrentX = 65
            End If
        Next i
                
        Picture2.Print
        Picture2.CurrentX = 150
        Picture2.ForeColor = vbWhite
        Picture2.Print "PUSH SPACE KEY"
        Timer1.Enabled = False
    End If
        
    Picture2.AutoRedraw = True

End Sub


'自機撃墜処理
Sub GekitsuiMe()
    cc = 2
    cb = 20
    Call PlaySound(BOMB1)
End Sub

'敵機撃墜処理
Sub GekitsuiEn(n As Integer, b As Single)
    Dim k As Integer, gsc As Long
    
    dd(n).dn = dd(n).dn - 1
    
    gsc = CLng(Int(CSng(dd(n).ds) * b))
    
    If dd(n).dn = 0 Then
        Select Case dd(n).dk
        Case 0
            geki(0) = geki(0) + 1
            teki_score(0) = teki_score(0) + gsc
        Case 1
            geki(1) = geki(1) + 1
            teki_score(1) = teki_score(1) + gsc
        Case 2
            geki(2) = geki(2) + 1
            teki_score(2) = teki_score(2) + gsc
        Case 3
            gsc = gsc + 300
            geki(4) = geki(4) + 1
        Case 4
            geki(3) = geki(3) + 1
        Case 5
            gsc = gsc + 60
            geki(3) = geki(3) + 1
        Case 6, 7
            geki(5) = geki(5) + 1
            teki_score(5) = teki_score(5) + gsc
        Case 10
            geki(6) = geki(6) + 1
            gsc = gsc + 1500
            BossScore = -1500
        End Select
        Call PlaySound(BOMB2)
    Else
        Call PlaySound(DAMAGE)
    End If
    
    Select Case dd(n).dk
    Case 3
        teki_score(4) = teki_score(4) + gsc
    Case 5
        teki_score(3) = teki_score(3) + gsc
    Case 10
        teki_score(6) = teki_score(6) + gsc
    End Select
    
    
    
    Score = Score + gsc
    
    BossScore = BossScore + gsc
    
    'ハイスコア
    If Score > HiScore Then
        HiScore = Score
        Me.Caption = "シューティングゲーム  HISCORE = " & HiScore
    End If
    
    '攻撃３ボーナス
    If Score - zs >= 350 Then
        zs = Score
        zn = zn + 1
    End If
    
    'ブースターボーナス
    bn = bn + 1
    If bn >= 20 Then
        bn = 0
        bb = bb + 1
    End If

    '撃墜描写の設定
    For k = 0 To 15
        If pp(k) = 0 Then
            pp(k) = 5
            If (dd(n).dn > 0) And ((dd(n).dk = 3) Or (dd(n).dk = 10)) Then
                pk(k) = 1
            Else
                pk(k) = 0
            End If
            px(k) = dd(n).dx
            py(k) = dd(n).dy
            pc(k) = dd(n).dc
            ps(k) = gsc
            pr(k) = dd(n).dr
            Exit For
        End If
    Next k
    
End Sub

'攻撃１（通常弾）の当たり判定
Sub HidanAttack1(n As Integer)
    Dim i As Integer, w As Single
    
    For i = 0 To 4
        If dd(i).dn > 0 Then
            w = Kyori(sx(n), sy(n), dd(i).dx, dd(i).dy)
            If w <= (sr + dd(i).dr) Then
                ss(n) = 0
                sn = sn - 1
                Call GekitsuiEn(i, 1)
                atari(0) = atari(0) + 1
                Exit For
            End If
        End If
    Next i
End Sub

'攻撃２（３方向弾）の当たり判定
Sub HidanAttack2(n As Integer)
    Dim i As Integer, w As Single
    
    For i = 0 To 4
        If dd(i).dn > 0 Then
            w = Kyori(tx(n), ty(n), dd(i).dx, dd(i).dy)
            If w <= (tr + dd(i).dr) Then
                tt(n) = 0
                tn = tn + 1
                Call GekitsuiEn(i, 0.6)
                atari(1) = atari(1) + 1
                Exit For
            End If
        End If
    Next i
End Sub

'攻撃３（貫通弾）の当たり判定
Sub HidanAttack3()
    Dim i As Integer, w As Single

    For i = 0 To 4
        If dd(i).dn > 0 Then
            w = Kyori(zx, zy, dd(i).dx, dd(i).dy)
            If w <= (zr + dd(i).dr) Then
                Call GekitsuiEn(i, 0.3)
                atari(2) = atari(2) + 1
            End If
        End If
    Next i
End Sub

'攻撃４（ミサイル）
Sub HidanAttack4(n As Integer)
    Dim w As Single, i As Integer
    
    Select Case uk(n)
    Case 1
        If dd(n).dn > 0 Then
            w = Kyori(ux1(n), uy1(n), dd(n).dx, dd(n).dy)
            If w <= (ur + dd(n).dr) Then
                uu(n) = 0
                ue = ue - 1
                Call GekitsuiEn(n, 0.3)
                atari(4) = atari(4) + 1
            End If
        End If
    Case 2, 3, 4
        For i = 0 To 4
            If dd(i).dn > 0 Then
                w = Kyori(ux1(n), uy1(n), dd(i).dx, dd(i).dy)
                If w <= (ur + dd(i).dr) Then
                    uu(n) = 0
                    ue = ue - 1
                    Call GekitsuiEn(i, 0.3)
                    atari(4) = atari(4) + 1
                    Exit Sub
                End If
            End If
        Next i
    End Select
    
End Sub

'敵弾の当たり判定
Sub HidanAttackE(n As Integer)
    Dim w As Single
    
    If vv > 0 Then
        w = Kyori(cx + 16, cy + 16, qx(n), qy(n))
        If CInt(w) <= (vr + qr(n)) Then
            qq(n) = 0
            atari(5) = atari(5) + 1
            Call PlaySound(GUARD)
            Exit Sub
        End If
    End If
    
    If cc < 2 Then
        w = Kyori(cx + 16, cy + 16, qx(n), qy(n))
        If CInt(w) <= (13 + qr(n)) Then
            Call GekitsuiMe
            qq(n) = 0
        End If
    End If
End Sub

'敵機の当たり判定
Sub HidanEnemy(n As Integer)
    Dim i As Integer, w As Single
    
    If sn > 0 Then
        For i = 0 To 9
            If ss(i) > 0 Then
                w = Kyori(sx(i), sy(i), dd(n).dx, dd(n).dy)
                If w <= (sr + dd(n).dr) Then
                    ss(i) = 0
                    sn = sn - 1
                    Call GekitsuiEn(n, 1)
                    atari(0) = atari(0) + 1
                    Exit Sub
                End If
            End If
        Next i
    End If
    
    If tn < 6 Then
        For i = 0 To 5
            If tt(i) > 0 Then
                w = Kyori(tx(i), ty(i), dd(n).dx, dd(n).dy)
                If w <= (tr + dd(n).dr) Then
                    tt(i) = 0
                    tn = tn + 1
                    Call GekitsuiEn(n, 0.6)
                    atari(1) = atari(1) + 1
                    Exit Sub
                End If
            End If
        Next i
    End If
    
    If zz > 0 Then
        w = Kyori(zx, zy, dd(n).dx, dd(n).dy)
        If w <= (zr + dd(n).dr) Then
            Call GekitsuiEn(n, 0.5)
            atari(2) = atari(2) + 1
            Exit Sub
        End If
    End If
    
    If ue > 0 Then
        Select Case uk(n)
        Case 1
            If uu(n) > 0 Then
                w = Kyori(ux1(n), uy1(n), dd(n).dx, dd(n).dy)
                If w <= (ur + dd(n).dr) Then
                    uu(n) = 0
                    ue = ue - 1
                    Call GekitsuiEn(n, 0.3)
                    atari(4) = atari(4) + 1
                    Exit Sub
                End If
            End If
        Case 2, 3, 4
            For i = 0 To 4
                If uu(i) > 0 Then
                    w = Kyori(ux1(i), uy1(i), dd(n).dx, dd(n).dy)
                    If w <= (ur + dd(n).dr) Then
                        uu(i) = 0
                        ue = ue - 1
                        Call GekitsuiEn(n, 0.3)
                        atari(4) = atari(4) + 1
                        Exit Sub
                    End If
                End If
            Next i
        End Select
    End If
    
    If vv > 0 Then
        w = Kyori(cx + 16, cy + 16, dd(n).dx, dd(n).dy)
        If CInt(w) <= CInt(vr + dd(n).dr) Then
            Call GekitsuiEn(n, 0.1)
            atari(5) = atari(5) + 1
            Exit Sub
        End If
    End If
    
    If cc < 2 Then
        w = Kyori(cx + 16, cy + 16, dd(n).dx, dd(n).dy)
        If CInt(w) <= CInt(13 + dd(n).dr) Then
            Call GekitsuiMe
        End If
    End If
    
End Sub



Private Sub Form_KeyPress(KeyAscii As Integer)
    Dim i As Integer, j As Integer
    
    If gFlag Then
        If KeyAscii = Asc(" ") Then
            gFlag = False
            mnuHiscoreClear.Enabled = False
            mnuSetting.Enabled = False
        End If
        Exit Sub
    End If
        
    If (cc = 2) And (KeyAscii = Asc(" ")) Then
        Call Init_Game
        Exit Sub
    End If
    
    If cc > 0 Then
        Exit Sub
    End If
    cc = 1
    
    Select Case KeyAscii
    Case Asc("i")
        cy = cy - (5 + bt)
        If bt > 0 Then
            bt = bt - 1
        End If
        If cy < -16 Then
            cy = -16
        End If
    Case Asc("k")
        cy = cy + (5 + bt)
        If bt > 0 Then
            bt = bt - 1
        End If
        If cy > (200 - 16) Then
            cy = 200 - 16
        End If
    Case Asc("j")
        cx = cx - (5 + bt)
        If bt > 0 Then
            bt = bt - 1
        End If
        If cx < -16 Then
            cx = -16
        End If
    Case Asc("l")
        cx = cx + (5 + bt)
        If bt > 0 Then
            bt = bt - 1
        End If
        If cx > (400 - 16) Then
            cx = 400 - 16
        End If
    Case Asc("f")
        If vv = 0 Then
            For i = 0 To 9
                If ss(i) = 0 Then
                    ss(i) = 1
                    sx(i) = cx + 16 + sr * 2
                    sy(i) = cy + 16
                    sn = sn + 1
                    hatsu(0) = hatsu(0) + 1
                    Call PlaySound(SHOT1)
                    Exit For
                End If
            Next i
        End If
    Case Asc("d")
        If (tn > 2) And (vv = 0) Then
            tn = tn - 3
            j = 0
            For i = 0 To 5
                If tt(i) = 0 Then
                    tt(i) = 1
                    tx(i) = cx + 16 + tr * 2
                    ty(i) = cy + 16 * j
                    tvy(i) = (j - 1) * 5
                    j = j + 1
                    If j = 3 Then
                        Exit For
                    End If
                End If
            Next i
            hatsu(1) = hatsu(1) + 3
            Call PlaySound(SHOT2)
        End If
    Case Asc("s")
        If (zn > 0) And (zz = 0) And (vv = 0) Then
            zn = zn - 1
            zz = 1
            zx = cx + 16 + zr * 2
            zy = cy + 16
            hatsu(2) = hatsu(2) + 1
            Call PlaySound(SHOT3)
        End If
    Case Asc("e")
        If (bb > 0) And (vv = 0) Then
            bb = bb - 1
            bt = 20
            atari(3) = atari(3) + 1
            hatsu(3) = hatsu(3) + 1
            Call PlaySound(SPEEDUP)
        End If
    Case Asc("a")
        If (um > 0) And (vv = 0) Then
            If ue = 0 Then
                um = um - 5
                ue = 5
                For i = 0 To 4
                    
                    uu(i) = 1
                    ux1(i) = 16 * Cos(6.28 * i / 5 - 1.57) + cx + 16
                    uy1(i) = 16 * Sin(6.28 * i / 5 - 1.57) + cy + 16
                    ux2(i) = cx + 16
                    uy2(i) = cy + 16
                    ux3(i) = ux2(i)
                    uy3(i) = uy2(i)
                    uvx(i) = 25 * (ux1(i) - ux2(i)) / 16
                    uvy(i) = 25 * (uy1(i) - uy2(i)) / 16
                    If dd(i).dn > 0 Then
                        uk(i) = 1
                    Else
                        uk(i) = 0
                    End If
                Next i
                hatsu(4) = hatsu(4) + 5
                Call PlaySound(SHOT4)
            End If
        End If
    Case Asc("g")
        If (um > 0) And (vv = 0) Then
            If ue = 0 Then
                um = um - 5
                ue = 5
                For i = 0 To 4
                    uu(i) = 1
                    ux1(i) = cx + 32 + 5 - i
                    uy1(i) = cy + i * 8
                    ux2(i) = cx + 32
                    uy2(i) = uy1(i)
                    ux3(i) = ux2(i)
                    uy3(i) = uy2(i)
                    uvx(i) = 35 - i * 2
                    uvy(i) = 0
                    uk(i) = 2
                Next i
                hatsu(4) = hatsu(4) + 5
                Call PlaySound(SHOT4)
            End If
        End If
    Case Asc("r")
        If (um > 0) And (vv = 0) Then
            If ue = 0 Then
                um = um - 5
                ue = 5
                For i = 0 To 4
                    uu(i) = 1
                    ux1(i) = 16 * Cos(-1.57 * i / 5) + cx + 16
                    uy1(i) = 16 * Sin(-1.57 * i / 5) + cy + 16
                    ux2(i) = cx + 16
                    uy2(i) = cy + 16
                    ux3(i) = ux2(i)
                    uy3(i) = uy2(i)
                    uvx(i) = 25 * (ux1(i) - ux2(i)) / 16
                    uvy(i) = 25 * (uy1(i) - uy2(i)) / 16
                    uk(i) = 3
                Next i
                hatsu(4) = hatsu(4) + 5
                Call PlaySound(SHOT4)
            End If
        End If
    Case Asc("w")
        If (um > 0) And (vv = 0) Then
            If ue = 0 Then
                um = um - 5
                ue = 5
                For i = 0 To 4
                    uu(i) = 1
                    ux1(i) = 16 * Cos(1.57 * i / 5) + cx + 16
                    uy1(i) = 16 * Sin(1.57 * i / 5) + cy + 16
                    ux2(i) = cx + 16
                    uy2(i) = cy + 16
                    ux3(i) = ux2(i)
                    uy3(i) = uy2(i)
                    uvx(i) = 25 * (ux1(i) - ux2(i)) / 16
                    uvy(i) = 25 * (uy1(i) - uy2(i)) / 16
                    uk(i) = 4
                Next i
                hatsu(4) = hatsu(4) + 5
                Call PlaySound(SHOT4)
            End If
        End If
    Case Asc("v")
        If (vn > 0) And (bt = 0) Then
            If vv = 0 Then
                vv = 30
                vn = vn - 1
                hatsu(5) = hatsu(5) + 1
                Call PlaySound(BARRIER)
            End If
        End If
    End Select
    
    Call Byosha
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Randomize
    
    sr = 4: sv = 15
    tr = 5: tv = 10
    zr = 10: zv = 25
    ur = 2: vr = 18
    
    teki_name(0) = "Cyan    "
    teki_name(1) = "Red     "
    teki_name(2) = "Magenta "
    teki_name(3) = "Blue    "
    teki_name(4) = "White   "
    teki_name(5) = "Orange  "
    teki_name(6) = "Green   "
    
    teki_max(0) = 250
    teki_max(1) = 150
    teki_max(2) = 40
    teki_max(3) = 30
    teki_max(4) = 10
    teki_max(5) = 100
    teki_max(6) = 3

    Hyoka = 3000
    
    wepon(0) = "F "
    wepon(1) = "D "
    wepon(2) = "S "
    wepon(3) = "E "
    wepon(4) = "m "
    wepon(5) = "V "
    
    For i = 0 To 79
        Stars(i).X = i * 5 + 1
        If (Rnd() * 100) > 20 Then
            Stars(i).k = 0
            If (Rnd() * 1000) > 980 Then
                Stars(i).C = RGB(255, 255, 128)
            ElseIf (Rnd() * 1000) > 980 Then
                Stars(i).C = RGB(128, 255, 255)
            ElseIf (Rnd() * 1000) > 980 Then
                Stars(i).C = RGB(255, 128, 255)
            Else
                Stars(i).C = vbWhite
            End If
        Else
            Stars(i).k = 1
            Stars(i).C = vbWhite
        End If
        Stars(i).Y = Rnd() * 200
        Stars(i).V = 4 + Stars(i).k
    Next i
    
    Call Init_Game
    
    Call Init_HiScoreFile
    
    
    HiScore = GetHiScore()
    
    Me.Caption = "シューティングゲーム  HISCORE = " & HiScore
    
    Call GetSoundFiles
    
End Sub

Private Sub Form_Paint()
    Call Byosha
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call SaveSoundFiles
    
    Call SaveHiScore(HiScore)
    
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuHelp_Click()
    Dim bln1 As Boolean
    
    bln1 = Timer1.Enabled
    Timer1.Enabled = False
    Form2.Show vbModal, Me
    
    Timer1.Enabled = bln1
    
End Sub

Private Sub mnuHiscoreClear_Click()
    
    If MsgBox("ハイスコアを０にします", vbOKCancel + vbExclamation, "確認") = vbCancel Then
        Exit Sub
    End If
    
    HiScore = 0
    Call SaveHiScore(0)
    
    Me.Caption = "シューティングゲーム  HISCORE = " & HiScore
    
End Sub


Private Sub mnuSetting_Click()
    Dim bln1 As Boolean
    
    bln1 = Timer1.Enabled
    Timer1.Enabled = False
    Form3.Show vbModal, Me
    
    Timer1.Enabled = bln1
    
End Sub

Private Sub Picture2_DblClick()
    Timer1.Enabled = Not (Timer1.Enabled)
End Sub

Private Sub Timer1_Timer()
    Dim X As Single, Y As Single, R As Single
    Dim i As Integer, d As Integer, j As Integer, k As Integer
    Dim ww As Single, n1 As Integer, n2 As Integer
    
    
    For i = 0 To 79
        Stars(i).X = Stars(i).X - Stars(i).V
        If Stars(i).X < 0 Then
            If (Rnd() * 100) > 20 Then
                Stars(i).k = 0
                If (Rnd() * 1000) > 980 Then
                    Stars(i).C = RGB(255, 255, 128)
                ElseIf (Rnd() * 1000) > 980 Then
                    Stars(i).C = RGB(128, 255, 255)
                ElseIf (Rnd() * 1000) > 980 Then
                    Stars(i).C = RGB(255, 128, 255)
                Else
                    Stars(i).C = vbWhite
                End If
            Else
                Stars(i).k = 1
                Stars(i).C = vbWhite
            End If
            Stars(i).X = 395 + Rnd() * 5
            Stars(i).Y = Rnd() * 200
            Stars(i).V = 4 + Stars(i).k
        End If
    Next i
    
    If gFlag Then
        Call Byosha
        Exit Sub
    End If
    
    If cc = 1 Then
        cc = 0
    End If
    
    
    'ゲームクリアチェック
    n1 = 0
    n2 = 0
    For i = 0 To 5
        n1 = n1 + teki_max(i)
        n2 = n2 + geki(i)
    Next i
    If n1 = n2 Then
        BossScore = 2000
        If geki(6) = teki_max(6) Then
            cc = 2
            Call Byosha
            MsgBox "敵を全滅させました！", , "おめでとう"
            Exit Sub
        End If
    End If
        
    '敵機発生処理
    X = Rnd() * 5 + 395
    Y = Rnd() * 200
    If cc < 2 Then
        d = CInt(Rnd() * 1000)
        For i = 0 To 4
            If dd(i).dn = 0 Then
                If BossScore > 1500 Then
                    If teki(6) > 0 Then
                        dd(i).dn = 160:   dd(i).dk = 10
                        dd(i).dx = 445:   dd(i).dy = 100
                        dd(i).dr = 50:   dd(i).ds = 5
                        dd(i).dc = RGB(128, 255, 128): dd(i).dv = 5
                        BossScore = -99999
                        teki(6) = teki(6) - 1
                        deru(6) = deru(6) + 1
                    Else
                        BossScore = 0
                    End If
                    Exit For
                End If
                
                If d > 990 Then
                    If teki(4) > 0 Then
                        dd(i).dn = 40:   dd(i).dk = 3
                        dd(i).dx = X:   dd(i).dy = Y / 2 + 50
                        dd(i).dr = 30:   dd(i).ds = 5
                        dd(i).dc = vbWhite:   dd(i).dv = 3
                        teki(4) = teki(4) - 1
                        deru(4) = deru(4) + 1
                    End If
                    Exit For
                ElseIf d > 970 Then
                    If teki(3) > 0 Then
                        dd(i).dn = 1:   dd(i).dk = 4
                        dd(i).dx = X:   dd(i).dy = Y
                        dd(i).dr = 16:   dd(i).ds = 10
                        dd(i).dc = vbCyan:   dd(i).dv = 10
                        teki(3) = teki(3) - 1
                        deru(3) = deru(3) + 1
                    End If
                    Exit For
                ElseIf d > 940 Then
                    If teki(2) > 0 Then
                        dd(i).dn = 1:   dd(i).dk = 2
                        dd(i).dx = X:   dd(i).dy = 80 * Sin(X / 20) + 100
                        dd(i).dr = 5:   dd(i).ds = 100
                        dd(i).dc = vbMagenta:   dd(i).dv = 5
                        teki(2) = teki(2) - 1
                        deru(2) = deru(2) + 1
                    End If
                    Exit For
                ElseIf d > 850 Then
                    If teki(1) > 0 Then
                        dd(i).dn = 1:   dd(i).dk = 1
                        dd(i).dx = X:   dd(i).dy = Y
                        dd(i).dr = 8:   dd(i).ds = 20
                        dd(i).dc = vbRed:   dd(i).dv = 20
                        teki(1) = teki(1) - 1
                        deru(1) = deru(1) + 1
                    End If
                    Exit For
                ElseIf d > 650 Then
                    If teki(5) > 0 Then
                        dd(i).dn = 1:   dd(i).dx = X
                        If (Rnd() * 100) > 50 Then
                            dd(i).dk = 6: dd(i).dy = 20
                        Else
                            dd(i).dk = 7: dd(i).dy = 180
                        End If
                        dd(i).dr = 10:   dd(i).ds = 30
                        dd(i).dc = RGB(255, 128, 0): dd(i).dv = 30
                        teki(5) = teki(5) - 1
                        deru(5) = deru(5) + 1
                    End If
                    Exit For
                ElseIf d > 400 Then
                    If teki(0) > 0 Then
                        dd(i).dn = 1:   dd(i).dk = 0
                        dd(i).dx = X:   dd(i).dy = Y
                        dd(i).dr = 16:  dd(i).ds = 10
                        dd(i).dc = vbCyan: dd(i).dv = 10
                        teki(0) = teki(0) - 1
                        deru(0) = deru(0) + 1
                    End If
                    Exit For
                Else
                    Exit For
                End If
            End If
        Next i
    End If
    
    '攻撃１の移動処理
    For i = 0 To 9
        If ss(i) > 0 Then
            sx(i) = sx(i) + sv
            If sx(i) > (400 + sr * 2) Then
                ss(i) = 0
                sn = sn - 1
            End If
            If ss(i) > 0 Then
                Call HidanAttack1(i)
            End If
        End If
    Next i
    
    '攻撃２の移動処理
    If tn < 6 Then
        For i = 0 To 5
            If tt(i) > 0 Then
                tx(i) = tx(i) + tv
                ty(i) = ty(i) + tvy(i)
                If (tx(i) > (400 + tr * 2)) _
                        Or (ty(i) < -tr * 2) Or (ty(i) > (200 + tr * 2)) Then
                    tt(i) = 0
                    tn = tn + 1
                End If
                If tt(i) > 0 Then
                    Call HidanAttack2(i)
                End If
            End If
        Next i
    End If
        
    '攻撃３の移動処理
    If zz > 0 Then
        zx = zx + zv
        If zx > (400 + zr * 2) Then
            zz = 0
        End If
        If zz > 0 Then
            Call HidanAttack3
        End If
    End If
    
    If vv > 0 Then
        vv = vv - 1
    End If
    
    '敵弾の移動処理
    For i = 0 To 20
        If qq(i) > 0 Then
            qx(i) = qx(i) + qvx(i)
            qy(i) = qy(i) + qvy(i)
            If (qx(i) < -qr(i)) Or (qy(i) < -qr(i)) _
                        Or (qy(i) > 200 + qr(i)) Then
                qq(i) = 0
            End If
            If qq(i) > 0 Then
                Call HidanAttackE(i)
            End If
        End If
    Next i
    
    '敵機の移動処理
    For i = 0 To 4
        If dd(i).dn > 0 Then
            dd(i).dx = dd(i).dx - dd(i).dv
            Select Case dd(i).dk
            Case 2
                dd(i).dy = 80 * Sin(dd(i).dx / 20) + 100
            Case 3
                j = dd(i).dn * 3 + 135
                dd(i).dc = RGB(j, j, j)
                If (Rnd() * 1000) < 10 Then
                    For j = 0 To 20
                        If qq(j) = 0 Then
                            qq(j) = 1
                            qx(j) = dd(i).dx
                            qy(j) = dd(i).dy
                            qr(j) = 2
                            qc(j) = vbCyan
                            qvx(j) = -(dd(i).dv + 10)
                            qvy(j) = 0
                            Call PlaySound(SHOT5)
                            Exit For
                        End If
                    Next j
                End If
            Case 4
                If dd(i).dx < 300 Then
                    dd(i).dk = 5
                    dd(i).dc = vbBlue
                    dd(i).dv = dd(i).dv * 2
                    dd(i).ds = dd(i).ds * 3
                    dd(i).dn = dd(i).dn + 2
                    For j = 0 To 20
                        If qq(j) = 0 Then
                            qq(j) = 1
                            qx(j) = dd(i).dx
                            qy(j) = dd(i).dy
                            qr(j) = 2
                            qc(j) = vbYellow
                            ww = Kyori(cx + 16, cy + 16, qx(j), qy(j))
                            qvx(j) = 25 * (cx + 16 - qx(j)) / ww
                            qvy(j) = 25 * (cy + 16 - qy(j)) / ww
                            Call PlaySound(SHOT5)
                            Exit For
                        End If
                    Next j
                End If
            Case 6
                dd(i).dy = dd(i).dy + 360 / dd(i).dv
            Case 7
                dd(i).dy = dd(i).dy - 360 / dd(i).dv
            Case 10
                If dd(i).dx <= 350 Then
                    dd(i).dx = 350
                    dd(i).dv = 0
                End If
                dd(i).dc = RGB(0, dd(i).dn + 95, 0)
                If (Rnd() * 1000) < 45 Then
                    For k = 0 To 2
                        For j = 0 To 20
                            If qq(j) = 0 Then
                                qq(j) = 1
                                qx(j) = dd(i).dx
                                qy(j) = dd(i).dy - 25 + k * 25
                                qr(j) = 2
                                qc(j) = vbCyan
                                qvx(j) = -20
                                qvy(j) = -5 + k * 5
                                Exit For
                            End If
                        Next j
                    Next k
                    Call PlaySound(SHOT5)
                ElseIf (Rnd() * 1000) < 20 Then
                    For k = 0 To 1
                        For j = 0 To 20
                            If qq(j) = 0 Then
                                qq(j) = 1
                                qx(j) = dd(i).dx
                                qy(j) = dd(i).dy - 25 + k * 50
                                qr(j) = 2
                                qc(j) = vbYellow
                                ww = Kyori(cx + 16, cy + 16, qx(j), qy(j))
                                qvx(j) = 20 * (cx + 16 - qx(j)) / ww
                                qvy(j) = 20 * (cy + 16 - qy(j)) / ww
                                Exit For
                            End If
                        Next j
                    Next k
                    Call PlaySound(SHOT5)
                End If
            End Select
            If dd(i).dx < -dd(i).dr * 2 Then
                dd(i).dn = 0
                Select Case dd(i).dk
                Case 0
                    teki(0) = teki(0) + 1
                Case 1
                    teki(1) = teki(1) + 1
                Case 2
                    teki(2) = teki(2) + 1
                Case 3
                    teki(4) = teki(4) + 1
                Case 4, 5
                    teki(3) = teki(3) + 1
                Case 6, 7
                    teki(5) = teki(5) + 1
                End Select
            End If
            If dd(i).dn > 0 Then
                Call HidanEnemy(i)
            End If
        End If
    Next i
    
    '攻撃４の移動処理
    If ue > 0 Then
        For i = 0 To 4
            If uu(i) > 0 Then
                ux3(i) = ux2(i)
                uy3(i) = uy2(i)
                ux2(i) = ux1(i)
                uy2(i) = uy1(i)
                ux1(i) = ux1(i) + uvx(i)
                uy1(i) = uy1(i) + uvy(i)
                If uk(i) = 1 Then
                    If dd(i).dn > 0 Then
                        ww = Kyori(ux1(i), uy1(i), dd(i).dx, dd(i).dy)
                        uvx(i) = 25 * (dd(i).dx - ux1(i)) / ww
                        uvy(i) = 25 * (dd(i).dy - uy1(i)) / ww
                    Else
                        uk(i) = 0
                    End If
                End If
                If uk(i) <> 1 Then
                    If (ux1(i) < 0) Or (ux1(i) > 400) _
                            Or (uy1(i) < 0) Or (uy1(i) > 200) Then
                        uu(i) = 0
                        ue = ue - 1
                    End If
                End If
                If (uu(i) > 0) Then
                    Call HidanAttack4(i)
                End If
            End If
        Next i
    End If
    
    '描写処理（自機、攻撃、敵機、スコア、残弾）
    Call Byosha
    
    '描写処理（敵機爆発、獲得スコア）
    Picture2.AutoRedraw = False
    For i = 0 To 15
        If pp(i) > 0 Then
            If pk(i) = 1 Then
                For j = 0 To CInt(pr(i))
                    X = Rnd() * pr(i) - pr(i) / 2 + px(i)
                    Y = Rnd() * pr(i) - pr(i) / 2 + py(i)
                    R = Rnd() * pr(i) / 2
                    Picture2.Circle (X, Y), R, RGB(128, 128, 128)
                Next j
            Else
                For j = 0 To CInt(pr(i)) + 2
                    X = Rnd() * pr(i) * 2 - pr(i) + px(i)
                    Y = Rnd() * pr(i) * 2 - pr(i) + py(i)
                    Picture2.PSet (X, Y), pc(i)
                Next j
            End If
            pp(i) = pp(i) - 1
            Picture2.CurrentX = px(i)
            Picture2.CurrentY = py(i)
            Picture2.ForeColor = pc(i)
            Picture2.Print ps(i)
            px(i) = px(i) - 5
        End If
    Next i
    Picture2.AutoRedraw = True
    
    
End Sub
